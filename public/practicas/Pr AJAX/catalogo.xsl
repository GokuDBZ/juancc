<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
	<section>
  			<h2>VIDEOJUEGOS</h2>
    <xsl:for-each select="Videojuegos/Videojuego">
    <article class="videojuegos">
      <div class="videojuegocaja">
      <img> <xsl:attribute name= "src"><xsl:value-of select="InfoGeneral/Foto" /> </xsl:attribute></img>
      <h3><xsl:value-of select="Nombre"/></h3>
      <div class="parrafo">
      <p><xsl:value-of select="InfoGeneral/Descripcion" /></p>
      </div> <a href="Detalle.html" class="info">más info</a>
    </div>
    </article>
    </xsl:for-each>
    </section>
</xsl:template>
</xsl:stylesheet>