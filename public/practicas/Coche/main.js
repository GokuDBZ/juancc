//Clases
class Coche {
  #id;

  get id() {
    return this.#id;
  }
  constructor(id) {
    this.#id = id;
  }
  crear() {
    let caja = document.createElement("div");
    caja.setAttribute('id', 'coche');
    document.body.append(caja);
    let coche = document.createElement("img");
    coche.setAttribute('src', 'coche.png');
    let selectCaja = document.body.querySelector("div");
    selectCaja.append(coche)
  }
}

class Camion {
  crear() {

    let camion = document.createElement("img");
    camion.setAttribute('class', 'camion');
    camion.setAttribute('src', 'camion.png');

    let posiciones = ["215px", "360px", "510px", "670px"];
    const posicionRandom = posiciones[Math.floor(Math.random() * posiciones.length)];

    camion.style.top = posicionRandom;

    document.body.append(camion);
  }
}
class Camion2 {
  crear() {
    let camion = document.createElement("img");
    camion.setAttribute('class', 'camion2');
    camion.setAttribute('src', 'coche2.png');

    let posiciones = ["215px", "360px", "510px", "670px"];
    const posicionRandom = posiciones[Math.floor(Math.random() * posiciones.length)];

    camion.style.top = posicionRandom;

    document.body.append(camion);
  }
}

let car = new Coche("coche");
car.crear();
let coche = document.getElementById(car.id);
let gameover = false;

//Spawn camiones
var allEnemies = [];
window.setInterval(function () {
  if (!gameover) {
    allEnemies.push(new Camion().crear());
    if (document.getElementsByClassName("camion").length >= 4) document.getElementsByClassName("camion")[0].remove();
  }

}, 2000);

//Spawn coches
window.setInterval(function () {
  if (!gameover) {


    allEnemies.push(new Camion2().crear());
    if (document.getElementsByClassName("camion2").length >= 3) document.getElementsByClassName("camion2")[0].remove();
  }
}, 2000);

//Funcion que comprueba si los objetos chocan
function overlaps(a, b) {
  const rect1 = a.getBoundingClientRect();
  const rect2 = b.getBoundingClientRect();
  const isInHoriztonalBounds =
    rect1.x < rect2.x + rect2.width && rect1.x + rect1.width > rect2.x;
  const isInVerticalBounds =
    rect1.y < rect2.y + rect2.height && rect1.y + rect1.height > rect2.y;
  const isOverlapping = isInHoriztonalBounds && isInVerticalBounds;
  return isOverlapping;
}

//Movimiento coche raton
const onMouseMove = (e) => {
  if (e.pageY > 200 && e.pageY < 760) {
    coche.style.top = e.pageY - 50 + 'px';
  }
}
document.addEventListener('mousemove', onMouseMove);

//Game Over
function gameOver() {
  gameover = true;
  var html = document.getElementsByTagName('html')[0];
  var body = document.getElementsByTagName('body')[0];
  html.removeChild(body);

  let newBody = document.createElement('body');

  document.documentElement.appendChild(newBody);
  newBody.setAttribute("class", "bg2");
  newBody.setAttribute("background", "gameOver.webp")
};

//Colision camiones
window.setInterval(function () {
  var enemigos = document.getElementsByClassName("camion");
  for (let index = 0; index < enemigos.length; index++) {
    if (overlaps(coche.firstChild, enemigos[index])) {
      coche.style.display = 'none';
      gameOver();
    }
  }
}, 50);

//Colision coche
window.setInterval(function () {
  var enemigos = document.getElementsByClassName("camion2");
  for (let index = 0; index < enemigos.length; index++) {
    if (overlaps(coche.firstChild, enemigos[index])) {
      coche.style.display = 'none';
      gameOver();
    }
  }
}, 10);

