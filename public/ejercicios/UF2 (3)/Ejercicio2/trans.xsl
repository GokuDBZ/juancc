<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    
<xsl:template match="/">
   <html>
        <body>
        <table border="1">
                <tr style="background-color: lime">
                    <th style="text-align:left">Foto</th>
                    <th style="text-align:left">Nombre</th>
                    <th style="text-align:left">Apellidos</th>
                    <th style="text-align:left">Telefono</th>
                    <th style="text-align:left">Repetidor</th>
                    <th style="text-align:left">Nota Practica</th>
                    <th style="text-align:left">Nota Examen</th>
                    <th style="text-align:left">Nota Total</th>
                </tr>
                <xsl:for-each select="evaluacion/alumno">
                <xsl:sort select="apellidos"/>
                <tr>
                    <xsl:choose>
                        <xsl:when test="imagen = ''"><td><img src="avatar.png" height="50px" width="50px"/></td></xsl:when>
                        <xsl:otherwise>
                            <td>
                            <img height="50px" width="50px">
                                <xsl:attribute name="src">
                                <xsl:value-of select="imagen"/>
                                </xsl:attribute>
                                </img>
                            </td>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <td><xsl:value-of select="nombre"></xsl:value-of></td>
                    <td><xsl:value-of select="apellidos"></xsl:value-of></td>
                    <td><xsl:value-of select="telefono"></xsl:value-of></td>
                    <td><xsl:value-of select="@repite"></xsl:value-of></td>
                    <td><xsl:value-of select="notas/practicas"></xsl:value-of></td>
                    <td><xsl:value-of select="notas/examen"></xsl:value-of></td>
                    <xsl:variable name="nota" select="(./notas/examen + ./notas/practicas) div 2"></xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$nota&gt;'7.9'"><td style="background-color: lightblue"><xsl:value-of select="$nota"></xsl:value-of></td></xsl:when>
                        <xsl:when test="$nota&lt;'5'"><td style="background-color: crimson"><xsl:value-of select="$nota"></xsl:value-of></td></xsl:when>
                        <xsl:otherwise><td><xsl:value-of select="$nota"></xsl:value-of></td></xsl:otherwise>
                    </xsl:choose>
                </tr>
                </xsl:for-each>
                
            </table>
        </body>
    </html>     
</xsl:template>
</xsl:stylesheet>