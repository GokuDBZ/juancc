<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    

<xsl:template match="/">
<html>
        <body>
            <xsl:for-each select="mundo/continente">
                <h1><xsl:value-of select="nombre"></xsl:value-of></h1>
                <xsl:variable name="continente" select="nombre"/>
                <xsl:for-each select="paises/pais">
                <xsl:sort select="nombre"/>
                    <table border="1">
                    <xsl:choose>  
                    <xsl:when test="$continente='Africa'"><xsl:attribute name="style"> border-color: green </xsl:attribute></xsl:when>
                    <xsl:when test="$continente='Europe'"><xsl:attribute name="style"> border-color: lightgrey </xsl:attribute></xsl:when>
                    <xsl:when test="$continente='Oceanía'"><xsl:attribute name="style"> border-color: blue </xsl:attribute></xsl:when>
                    <xsl:when test="$continente='Asia'"><xsl:attribute name="style"> border-color: yellow </xsl:attribute></xsl:when>
                    <xsl:when test="$continente='America'"><xsl:attribute name="style"> border-color: red </xsl:attribute></xsl:when>
                    </xsl:choose>

                    <tr>
                        <th style="text-align:left">Bandera</th>
                        <th style="text-align:left">País</th>
                        <th style="text-align:left">Gobierno</th>
                        <th style="text-align:left">Capital</th>
                    </tr>
                    <tr>
                        <td>
                        <img height="50px" width="50px" src="{concat('img/',foto)}">
                        </img>
                        </td>            
                        <td><xsl:value-of select="nombre"></xsl:value-of></td>
                        <xsl:variable name="gobierno" select="nombre/@gobierno"/>
                        <xsl:choose>
                            <xsl:when test="$gobierno='monarquia'"><td style="background-color: yellow"><xsl:value-of select="$gobierno"></xsl:value-of></td></xsl:when>
                            <xsl:when test="$gobierno='dictadura'"><td style="background-color: red"><xsl:value-of select="$gobierno"></xsl:value-of></td></xsl:when>
                            <xsl:otherwise><td><xsl:value-of select="$gobierno"></xsl:value-of></td></xsl:otherwise>
                        </xsl:choose>
                        <td><xsl:value-of select="capital"></xsl:value-of></td>
                    </tr>
                    </table>
                </xsl:for-each>
            </xsl:for-each>
        </body>
    </html>     
</xsl:template>
</xsl:stylesheet>