1. 

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    

<xsl:template match="/">
    <xsl:for-each select="mundo/continente/paises/pais">
            <xsl:value-of select="nombre"></xsl:value-of>
    </xsl:for-each>
</xsl:template>
</xsl:stylesheet>

2.

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes"/>    

<xsl:template match="/">
    <xsl:for-each select="mundo/continente/paises/pais">
            <xsl:value-of select="nombre[@gobierno='republica']"></xsl:value-of>
    </xsl:for-each>
</xsl:template>
</xsl:stylesheet>